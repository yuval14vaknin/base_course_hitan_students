package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("ejectedPilotRescue/")
public class EjectedPilotRescueRestController {

  @Autowired private CrudDataBase dataBase;

  @Autowired private AirplanesAllocationManager airplanesAllocationManager;

  @GetMapping("infos")
  public List<EjectedPilotInfo> getPilots() {
    return dataBase.getAllOfType(EjectedPilotInfo.class);
  }

  @GetMapping("takeResponsibility")
  public void getResponsibility(
          @RequestParam("ejectionId") int ejectionId,
          @CookieValue(value = "client-id", defaultValue = "") String clientId) {

    EjectedPilotInfo pilot = dataBase.getByID(ejectionId, EjectedPilotInfo.class);
    if (pilot.getRescuedBy() == null) {
      pilot.setRescuedBy(clientId);
      dataBase.update(pilot);
      airplanesAllocationManager.allocateAirplanesForEjection(pilot, clientId);
    }
  }
}
